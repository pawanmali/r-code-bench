

library(shiny)
library(shinydashboard)
library(prettyR)
library(R2HTML)
library(shinyAce)
library(shinyBS)

addResourcePath("temp", "E:/Code/R/mar-04-r-code-bench/www/temp/")

ui <- fluidPage(dashboardPage(
  dashboardHeader(title = "Code Bench"),
  
  dashboardSidebar(
    br(),
    #### Sidebar ----
    
    ## Import data button  ----
    actionButton(
      inputId = "importdata",
      label = "Import data",
      icon = shiny::icon("database"),
      width = "70%"
    ),
    br(),
    ## Load Script button  ----
    actionButton(
      inputId = "importscript",
      label = "Load Script ",
      icon = shiny::icon("file-upload"),
      width = "70%"
    ),
    br()
    
  ),
  dashboardBody(
    tags$style(
      ".shiny-notification {
                                   position:fixed;
                                   top: calc(20%);
                                   left: calc(20%);
                                   }"
    ),
    fluidRow(
      #### BS Modals ----
      # * Input Data Ui 
      bsModal(
        "importdata_model",
        "Import data",
        "importdata",
        size = "small",
        fileInput(
          'datafile',
          label = "",
          accept = c(".csv", ".xlsx", ".xls", ".json")
        ),
        hr(),
        actionButton("Data_Submit", "Submit")
      ),
      # * Input Script Ui
      bsModal(
        "import_script_model",
        "Import Script",
        "importscript",
        size = "small",
        fileInput(
          'scriptfile',
          label = "",
          accept = c(".r", ".py")
        ),
        hr(),
        actionButton("Script_Submit", "Submit")
      )
      
      
    ),
    fluidRow(
      column(
        6,
        # * Ui Execute Button ----
        
        actionButton(
          inputId = "actionButton_1",
          label = "  Execute",
          icon = shiny::icon("play")
        ),
        actionButton(
          inputId = "reset_button",
          label = "Reset All",
          icon = shiny::icon("trash-alt")
        ),
        actionButton(
          inputId = "actionButton_3",
          label = "Settings",
          icon = shiny::icon("cog")
        ),
        actionButton(
          inputId = "show",
          label = "",
          icon = shiny::icon("info")
        ),
        br(),
        br(),
        
        # * Ui Code Editor ----
        shinyAce::aceEditor(
          outputId = "Coding_editor",
          theme = "chrome",
          mode = "r",
          height = "600px",
          tabSize = 4,
          selectionId = "selection",
          value = "",
          placeholder = paste(
            "type your code here...",
            "1. add ; after each R line ",
            "2. add #--FIG-- after the plot command",
            "3. Press i button for more information",
            sep = '\n'
          )
        )
        
      ),
      column(
        6,
        # * Ui HTML Output  ----
        
        downloadButton(
          outputId = "downloadData",
          label = "Download Output",
          icon = shiny::icon("download")
        ),
        
        br(),
        br(),
        htmlOutput('htmlOutput_2')
      )
      
    )
    
  )
))



server <- function(input, output, session) {
  
  #### Default Start State ----
  cwd <- getwd()
  setwd("E:/Code/R/mar-04-r-code-bench/www/temp/")
  on.exit(setwd(cwd))
  code_text <- " "
  code_text <- paste(code_text, "\n", sep = "")
  
  rcon <- file("code.R", "w")
  cat(code_text, file = rcon)
  close(rcon)
  R2html(
    "code.R",
    "out_html.html",
    browse = FALSE,
    title = "",
    bgcolor = "#FFFFFF",
    split = FALSE
  )
  
  
  output$htmlOutput_2 <- renderUI({
    tags$iframe(
      seamless = "seamless",
      src = "temp/out_html_list.html",
      height = 600,
      width = 700
    )
  })
  
  # * Notification ----
  observeEvent(input$show, {
    showNotification(
      paste(
        "Add ; after each R line ",
        " and add #--FIG-- after the plot command",
        sep = '\n'
      ),
      type = "message"
    )
  })
  
  # * Execute button ----
  
  observeEvent(input$actionButton_1, {
    cwd <- getwd()
    setwd("E:/Code/R/mar-04-r-code-bench/www/temp/")
    
    on.exit(setwd(cwd))
    code_text <- input$Coding_editor
    code_text <- gsub("[\n]", " ", code_text)
    code_text <- gsub("[\r]", " ", code_text)
    print(code_text)
    code_text <- gsub(";", "\n", code_text)
    print(code_text)
    code_text <- paste(code_text, "\n", sep = "")
    
    print(code_text)
    rcon <- file("code.R", "w")
    cat(code_text, file = rcon)
    
    close(rcon)
    R2html(
      "code.R",
      "out_html.html",
      browse = FALSE,
      title = "",
      bgcolor = "#FFFFFF",
      split = FALSE
    )
    
    output$htmlOutput_2 <- renderUI({
      tags$iframe(
        seamless = "seamless",
        src = "temp/out_html_list.html",
        height = 600,
        width = 700
      )
      
      
      
    })
    
  })
  
  # * Reset All button ----
  
  observeEvent(input$reset_button, {
    updateAceEditor(session, "Coding_editor", value = "")
    # the following line to clean up,
    print("Clearing all temp files")
    system("rm code.R out_html.html out_html_list.html ")
    system("rm testR2html_list.html ")
    
    cwd <- getwd()
    setwd("E:/Code/R/mar-04-r-code-bench/www/temp/")
    on.exit(setwd(cwd))
    code_text <- " "
    code_text <- paste(code_text, "\n", sep = "")
    
    rcon <- file("code.R", "w")
    cat(code_text, file = rcon)
    close(rcon)
    R2html(
      "code.R",
      "out_html.html",
      browse = FALSE,
      title = "",
      bgcolor = "#FFFFFF",
      split = FALSE
    )
    
    output$htmlOutput_2 <- renderUI({
      tags$iframe(
        seamless = "seamless",
        src = "temp/out_html_list.html",
        height = 600,
        width = 700
      )
      
      
      
    })
    
  })
  
  
  # * Download button Output ----
  
  
  
  output$downloadData <- downloadHandler(filename <- function() {
    paste("output_", Sys.Date(), ".html", sep = "")
  },
  
  content <- function(file) {
    file.copy("www/temp/out_html_list.html", file)
  },
  contentType = "application/html")
  
  # * Data Import ----
  observeEvent(input$Data_Submit, {
    print("Importing the data")
    Raw_Data <- NULL
    
    inFile = input$datafile
    file_path <- inFile$datapath
    file_path <- gsub('\\\\', "/", file_path)
      #Raw_Data <- read.csv(inFile$datapath)
    print(file_path)
    updateAceEditor(session,
                    "Coding_editor",
                    value = paste0("data <- read.csv('", file_path, "');\n",
                                   "print(head(data));\n",
                                   "print(summary(data));\n"))
    
  }, ignoreInit = TRUE)
  
  
  # * Script Import ----
  observeEvent(input$Script_Submit, {
    print("Importing the Script")
    Raw_Script <- NULL
    
    inFile <- input$scriptfile
    rcon<-file(inFile$datapath,"rb")
    Raw_Script <- readLines(rcon)
    close(rcon)
    updateAceEditor(session,
                    "Coding_editor",
                    value = paste(Raw_Script,collapse = "\n"))

  
  })
  
}

# Run the application
shinyApp(ui = ui, server = server)
